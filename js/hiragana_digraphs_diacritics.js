﻿const hiragana_digraphs_diacritics = {
    ya: {
        g: { ENG: "gya", JPN: "ぎゃ", TYPE: "hiragana_digraphs_diacritics" },
        z: { ENG: "ja", JPN: "じゃ", TYPE: "hiragana_digraphs_diacritics" },
        d: { ENG: "ja", JPN: "ぢゃ", TYPE: "hiragana_digraphs_diacritics" },
        b: { ENG: "bya", JPN: "びゃ", TYPE: "hiragana_digraphs_diacritics" },
        p: { ENG: "pya", JPN: "ぴゃ", TYPE: "hiragana_digraphs_diacritics" },
    },
    yu: {
        g: { ENG: "gyu", JPN: "ぎゅ", TYPE: "hiragana_digraphs_diacritics" },
        z: { ENG: "ju", JPN: "じゅ", TYPE: "hiragana_digraphs_diacritics" },
        d: { ENG: "ju", JPN: "ぢゅ", TYPE: "hiragana_digraphs_diacritics" },
        b: { ENG: "byu", JPN: "びゅ", TYPE: "hiragana_digraphs_diacritics" },
        p: { ENG: "pyu", JPN: "ぴゅ", TYPE: "hiragana_digraphs_diacritics" },
    },
    yo: {
        g: { ENG: "gyo", JPN: "ぎょ", TYPE: "hiragana_digraphs_diacritics" },
        z: { ENG: "jo", JPN: "じょ", TYPE: "hiragana_digraphs_diacritics" },
        d: { ENG: "jo", JPN: "ぢょ", TYPE: "hiragana_digraphs_diacritics" },
        b: { ENG: "byo", JPN: "びょ", TYPE: "hiragana_digraphs_diacritics" },
        p: { ENG: "pyo", JPN: "ぴょ", TYPE: "hiragana_digraphs_diacritics" },
    }
};

create_table('hiragana-digraphs-diacritics', hiragana_digraphs_diacritics);
