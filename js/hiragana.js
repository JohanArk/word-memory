﻿const elem_ids = [];

const hiragana = {
    a: {
        _: { ENG: "a", JPN: "あ", TYPE: "hiragana" },
        k: { ENG: "ka", JPN: "か", TYPE: "hiragana" },
        s: { ENG: "sa", JPN: "さ", TYPE: "hiragana" },
        t: { ENG: "ta", JPN: "た", TYPE: "hiragana" },
        n: { ENG: "na", JPN: "な", TYPE: "hiragana" },
        h: { ENG: "ha", JPN: "は", TYPE: "hiragana" },
        m: { ENG: "ma", JPN: "ま", TYPE: "hiragana" },
        y: { ENG: "ya", JPN: "や", TYPE: "hiragana" },
        r: { ENG: "ra", JPN: "ら", TYPE: "hiragana" },
        w: { ENG: "wa", JPN: "わ", TYPE: "hiragana" },
        _n: { ENG: "n", JPN: "ん", TYPE: "hiragana" }
    },
    i: {
        _: { ENG: "i", JPN: "い", TYPE: "hiragana" },
        k: { ENG: "ki", JPN: "き", TYPE: "hiragana" },
        s: { ENG: "shi", JPN: "し", TYPE: "hiragana" },
        t: { ENG: "chi", JPN: "ち", TYPE: "hiragana" },
        n: { ENG: "ni", JPN: "に", TYPE: "hiragana" },
        h: { ENG: "hi", JPN: "ひ", TYPE: "hiragana" },
        m: { ENG: "mi", JPN: "み", TYPE: "hiragana" },
        y: undefined,
        r: { ENG: "ri", JPN: "り", TYPE: "hiragana" },
        w: undefined,
        _n: undefined
    },
    u: {
        _: { ENG: "u", JPN: "う", TYPE: "hiragana" },
        k: { ENG: "ku", JPN: "く", TYPE: "hiragana" },
        s: { ENG: "su", JPN: "す", TYPE: "hiragana" },
        t: { ENG: "tsu", JPN: "つ", TYPE: "hiragana" },
        n: { ENG: "nu", JPN: "ぬ", TYPE: "hiragana" },
        h: { ENG: "fu", JPN: "ふ", TYPE: "hiragana" },
        m: { ENG: "mu", JPN: "む", TYPE: "hiragana" },
        y: { ENG: "yu", JPN: "ゆ", TYPE: "hiragana" },
        r: { ENG: "ru", JPN: "る", TYPE: "hiragana" },
        w: undefined,
        _n: undefined
    },
    e: {
        _: { ENG: "e", JPN: "え", TYPE: "hiragana" },
        k: { ENG: "ke", JPN: "け", TYPE: "hiragana" },
        s: { ENG: "se", JPN: "せ", TYPE: "hiragana" },
        t: { ENG: "te", JPN: "て", TYPE: "hiragana" },
        n: { ENG: "ne", JPN: "ね", TYPE: "hiragana" },
        h: { ENG: "he", JPN: "へ", TYPE: "hiragana" },
        m: { ENG: "me", JPN: "め", TYPE: "hiragana" },
        y: undefined,
        r: { ENG: "re", JPN: "れ", TYPE: "hiragana" },
        w: undefined,
        _n: undefined
    },
    o: {
        _: { ENG: "o", JPN: "お", TYPE: "hiragana" },
        k: { ENG: "ko", JPN: "こ", TYPE: "hiragana" },
        s: { ENG: "so", JPN: "そ", TYPE: "hiragana" },
        t: { ENG: "to", JPN: "と", TYPE: "hiragana" },
        n: { ENG: "no", JPN: "の", TYPE: "hiragana" },
        h: { ENG: "ho", JPN: "ほ", TYPE: "hiragana" },
        m: { ENG: "mo", JPN: "も", TYPE: "hiragana" },
        y: { ENG: "yo", JPN: "よ", TYPE: "hiragana" },
        r: { ENG: "ro", JPN: "ろ", TYPE: "hiragana" },
        w: { ENG: "wo", JPN: "を", TYPE: "hiragana" },
        _n: undefined
    },
};

create_table('hiragana-table', hiragana);
