
function create_table(table_name, kana_obj) {
    // create table
    var table_element = document.getElementById(table_name);

    var column_row = table_element.insertRow(0);
    var th_element_2 = document.createElement("TH");
    var t_text_2 = document.createTextNode("*");

    th_element_2.setAttribute("onclick", "select_all(" + '"' + table_name + '"' + ")");
    th_element_2.setAttribute("id", "s_all" + "_" + table_name);
    th_element_2.setAttribute("class", table_name);

    th_element_2.appendChild(t_text_2);
    column_row.appendChild(th_element_2);

    var is_create_top_row = true;

    for (var key_row in kana_obj) {
        if (kana_obj.hasOwnProperty(key_row)) {
            // create row...
            var table_row_element = document.createElement("TR");

            var th_element = document.createElement("TH");
            th_element.setAttribute("onclick", "select_row(" + '"' + key_row + '",' + '"' + table_name + '"' +")");
            th_element.setAttribute("id", key_row + "_" + table_name);
            th_element.setAttribute("class", table_name);

            var t_text = document.createTextNode(key_row + "-");
            th_element.appendChild(t_text);
            table_row_element.appendChild(th_element);

            document.getElementById(table_name).appendChild(table_row_element);

            for (var key_column in kana_obj[key_row]) {
                if (kana_obj[key_row].hasOwnProperty(key_column)) {
                    if (is_create_top_row) {
                        var th_element_2 = document.createElement("TH");
                        th_element_2.setAttribute("onclick", "select_column(" + '"' + key_column + '",' + '"' + table_name + '"' + ")");
                        th_element_2.setAttribute("id", key_column + "_" + table_name);
                        th_element_2.setAttribute("class", table_name);

                        var t_text_2 = "";

                        if (key_column == "_") {
                            t_text_2 = document.createTextNode("[ ]");
                        }
                        else if (key_column == "_n") {
                            t_text_2 = document.createTextNode("n");
                        }
                        else {
                            t_text_2 = document.createTextNode(key_column + "-");
                        }

                        th_element_2.appendChild(t_text_2);
                        column_row.appendChild(th_element_2);
                    }

                    // create column...

                    character = kana_obj[key_row][key_column];

                    if (character !== undefined) {

                        // create cell...
                        var td_element = document.createElement("TD");
                        td_element.innerHTML = `${character.JPN} <span>${character.ENG}</span>`;

                        table_row_element.appendChild(td_element);

                        var id = key_row + "_" + key_column;

                        elem_ids.push({id: id, kana_obj: kana_obj});

                        td_element.setAttribute("onclick", "select(" + '"' + key_row + '", "' + key_column + '", "' + id + '")');
                        td_element.setAttribute("id", id);
                        td_element.setAttribute("class", table_name);
                    }
                    else {
                        // create empty cell...
                        var td_element = document.createElement("TD");
                        td_element.setAttribute("class", "empty");
                        var t_text = document.createTextNode("");
                        td_element.appendChild(t_text);
                        table_row_element.appendChild(td_element);
                    }
                }
            }
            is_create_top_row = false;
        }
    }
}

function select(a_row, a_column, id) {
    clicked_element = document.getElementById(id);

    if (clicked_element.classList.contains("selected")) {
        clicked_element.classList.remove("selected");
    }
    else {
        clicked_element.classList.add("selected");
    }
}

function select_all(a_class_name) {
    let is_selecting = false;

    select_all_element = document.getElementById("s_all" + "_" + a_class_name);

    if (select_all_element.classList.contains("selected_tab")) {
        select_all_element.classList.remove("selected_tab");
    }
    else {
        select_all_element.classList.add("selected_tab");

        is_selecting = true;
    }

    for (i = 0; i < elem_ids.length; i++) {
        element = document.getElementById(elem_ids[i]["id"]);

        if (element.classList.contains(a_class_name)) {
            if (is_selecting) {
                element.classList.add("selected");
            }
            else {
                element.classList.remove("selected");
            }
        }
    }
}

function select_row(a_row, a_class_name) {
    let is_selecting = false;

    select_row_element = document.getElementById(a_row + "_" + a_class_name);

    if (select_row_element.classList.contains("selected_tab")) {
        select_row_element.classList.remove("selected_tab");
    }
    else {
        select_row_element.classList.add("selected_tab");

        is_selecting = true;
    }

    for (i = 0; i < elem_ids.length; i++) {

        let middle = elem_ids[i]["id"].indexOf("_");

        let row = elem_ids[i]["id"].substr(0, middle);

        if (row == a_row) {
            element = document.getElementById(elem_ids[i]["id"]);

            if (element.classList.contains(a_class_name)) {
                if (is_selecting) {
                    element.classList.add("selected");
                }
                else {
                    element.classList.remove("selected");
                }
            }
        }
    }
}

function select_column(a_column, a_class_name) {
    if (a_column == "_n") {
        var select_column_element = document.getElementById(a_column + "_" + a_class_name);

        if (select_column_element.classList.contains("selected_tab")) {
            select_column_element.classList.remove("selected_tab");
            document.getElementById("a__n").classList.remove("selected");
        }
        else {
            select_column_element.classList.add("selected_tab");
            document.getElementById("a__n").classList.add("selected");
        }
    }
    else {
        let is_selecting = false;

        var select_column_element = document.getElementById(a_column + "_" + a_class_name);

        if (select_column_element.classList.contains("selected_tab")) {
            select_column_element.classList.remove("selected_tab");
        }
        else {
            select_column_element.classList.add("selected_tab");

            is_selecting = true;
        }

        for (i = 0; i < elem_ids.length; i++) {

            let middle = elem_ids[i]["id"].indexOf("_");
            let column = elem_ids[i]["id"].substr(middle + 1, elem_ids[i]["id"].length);

            if (column == a_column) {
                if (elem_ids[i]["id"].length >= 3) {
                    element = document.getElementById(elem_ids[i]["id"]);

                    if (element.classList.contains(a_class_name)) {
                        if (is_selecting) {
                            element.classList.add("selected");
                        }
                        else {
                            element.classList.remove("selected");
                        }

                    }
                }
            }
        }
    }
}