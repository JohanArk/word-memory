﻿const hiragana_diacritics = {
    a: {
        g: { ENG: "ga", JPN: "が", TYPE: "hiragana_diacritics" },
        z: { ENG: "za", JPN: "ざ", TYPE: "hiragana_diacritics" },
        d: { ENG: "da", JPN: "だ", TYPE: "hiragana_diacritics" },
        b: { ENG: "ba", JPN: "ば", TYPE: "hiragana_diacritics" },
        p: { ENG: "pa", JPN: "ぱ", TYPE: "hiragana_diacritics" },
    },
    i: {
        g: { ENG: "gi", JPN: "ぎ", TYPE: "hiragana_diacritics" },
        z: { ENG: "ji", JPN: "じ", TYPE: "hiragana_diacritics" },
        d: { ENG: "ji", JPN: "ぢ", TYPE: "hiragana_diacritics" },
        b: { ENG: "bi", JPN: "び", TYPE: "hiragana_diacritics" },
        p: { ENG: "pi", JPN: "ぴ", TYPE: "hiragana_diacritics" },
    },
    u: {
        g: { ENG: "gu", JPN: "ぐ", TYPE: "hiragana_diacritics" },
        z: { ENG: "zu", JPN: "ず", TYPE: "hiragana_diacritics" },
        d: { ENG: "zu", JPN: "づ", TYPE: "hiragana_diacritics" },
        b: { ENG: "bu", JPN: "ぶ", TYPE: "hiragana_diacritics" },
        p: { ENG: "pu", JPN: "ぷ", TYPE: "hiragana_diacritics" },
    },
    e: {
        g: { ENG: "ge", JPN: "げ", TYPE: "hiragana_diacritics" },
        z: { ENG: "ze", JPN: "ぜ", TYPE: "hiragana_diacritics" },
        d: { ENG: "de", JPN: "で", TYPE: "hiragana_diacritics" },
        b: { ENG: "be", JPN: "べ", TYPE: "hiragana_diacritics" },
        p: { ENG: "pe", JPN: "ぺ", TYPE: "hiragana_diacritics" },
    },
    o: {
        g: { ENG: "go", JPN: "ご", TYPE: "hiragana_diacritics" },
        z: { ENG: "zo", JPN: "ぞ", TYPE: "hiragana_diacritics" },
        d: { ENG: "do", JPN: "ど", TYPE: "hiragana_diacritics" },
        b: { ENG: "bo", JPN: "ぼ", TYPE: "hiragana_diacritics" },
        p: { ENG: "po", JPN: "ぽ", TYPE: "hiragana_diacritics" },
    },
};

create_table('hiragana-diacritics-table', hiragana_diacritics);
