﻿const hiragana_digraphs = {
    ya: {
        k: { ENG: "kya", JPN: "きゃ", TYPE: "hiragana_digraphs" },
        s: { ENG: "sha", JPN: "しゃ", TYPE: "hiragana_digraphs"  },
        t: { ENG: "cha", JPN: "ちゃ", TYPE: "hiragana_digraphs"  },
        n: { ENG: "nya", JPN: "にゃ", TYPE: "hiragana_digraphs"  },
        h: { ENG: "hya", JPN: "ひゃ", TYPE: "hiragana_digraphs"  },
        m: { ENG: "mya", JPN: "みゃ", TYPE: "hiragana_digraphs"  },
        r: { ENG: "rya", JPN: "りゃ", TYPE: "hiragana_digraphs"  }
    },
    yu: {
        k: { ENG: "kyu", JPN: "きゅ", TYPE: "hiragana_digraphs"  },
        s: { ENG: "shu", JPN: "しゅ", TYPE: "hiragana_digraphs"  },
        t: { ENG: "chu", JPN: "ちゅ", TYPE: "hiragana_digraphs"  },
        n: { ENG: "nyu", JPN: "にゅ", TYPE: "hiragana_digraphs"  },
        h: { ENG: "hyu", JPN: "ひゅ", TYPE: "hiragana_digraphs"  },
        m: { ENG: "myu", JPN: "みゅ", TYPE: "hiragana_digraphs"  },
        r: { ENG: "ryu", JPN: "りゅ", TYPE: "hiragana_digraphs"  }
    },
    yo: {
        k: { ENG: "kyo", JPN: "きょ", TYPE: "hiragana_digraphs"  },
        s: { ENG: "sho", JPN: "しょ", TYPE: "hiragana_digraphs"  },
        t: { ENG: "cho", JPN: "ちょ", TYPE: "hiragana_digraphs"  },
        n: { ENG: "nyo", JPN: "にょ", TYPE: "hiragana_digraphs"  },
        h: { ENG: "hyo", JPN: "ひょ", TYPE: "hiragana_digraphs"  },
        m: { ENG: "myo", JPN: "みょ", TYPE: "hiragana_digraphs"  },
        r: { ENG: "ryo", JPN: "りょ", TYPE: "hiragana_digraphs"  }
    }
};

create_table('hiragana-digraphs', hiragana_digraphs);
