﻿const hiragana = [];
const h_digraphs = [];
const h_diacritics = [];
const h_digraphs_diacritics = [];
const katakana = [];
const kanji = [];

function add_letter(a_script, a_eng, a_jap)
{
    if (a_script == "Hiragana") {
        hiragana.push({ script: a_script, eng: a_eng, jap: a_jap });
    }
    else if (a_script == "H_Digraphs") {
        h_digraphs.push({ script: a_script, eng: a_eng, jap: a_jap });
    }
    else if (a_script == "H_Diacritics") {
        h_diacritics.push({ script: a_script, eng: a_eng, jap: a_jap });
    }
    else if (a_script == "H_Digraphs_Diacritics") {
        h_digraphs_diacritics.push({ script: a_script, eng: a_eng, jap: a_jap });
    }
    else if (a_script == "Katakana") {
        katakana.push({ script: a_script, eng: a_eng, jap: a_jap });
    }
    else if (a_script == "Kanji")
    {
        kanji.push({ script: a_script, eng: a_eng, jap: a_jap });
    }

    //console.log(`Added ${a_script} - ${a_eng} : ${a_jap}`);
}

add_letter("Hiragana", "a", "あ");
add_letter("Hiragana", "i", "い");
add_letter("Hiragana", "u", "う");
add_letter("Hiragana", "e", "え");
add_letter("Hiragana", "o", "お");

add_letter("Hiragana", "ka", "か");
add_letter("Hiragana", "ki", "き");
add_letter("Hiragana", "ku", "く");
add_letter("Hiragana", "ke", "け");
add_letter("Hiragana", "ko", "こ");

add_letter("Hiragana", "sa", "さ");
add_letter("Hiragana", "shi", "し");
add_letter("Hiragana", "su", "す");
add_letter("Hiragana", "se", "せ");
add_letter("Hiragana", "so", "そ");

add_letter("Hiragana", "ta", "た");
add_letter("Hiragana", "chi", "ち");
add_letter("Hiragana", "tsu", "つ");
add_letter("Hiragana", "te", "て");
add_letter("Hiragana", "to", "と");

add_letter("Hiragana", "na", "な");
add_letter("Hiragana", "ni", "に");
add_letter("Hiragana", "nu", "ぬ");
add_letter("Hiragana", "ne", "ね");
add_letter("Hiragana", "no", "の");

add_letter("Hiragana", "ha", "は");
add_letter("Hiragana", "hi", "ひ");
add_letter("Hiragana", "fu", "ふ");
add_letter("Hiragana", "he", "へ");
add_letter("Hiragana", "ho", "ほ");

add_letter("Hiragana", "ma", "ま");
add_letter("Hiragana", "mi", "み");
add_letter("Hiragana", "mu", "む");
add_letter("Hiragana", "me", "め");
add_letter("Hiragana", "mo", "も");

add_letter("Hiragana", "ra", "ら");
add_letter("Hiragana", "ri", "り");
add_letter("Hiragana", "ru", "る");
add_letter("Hiragana", "re", "れ");
add_letter("Hiragana", "ro", "ろ");

add_letter("Hiragana", "ya", "や");
add_letter("Hiragana", "yu", "ゆ");
add_letter("Hiragana", "yo", "よ");
add_letter("Hiragana", "wa", "わ");
add_letter("Hiragana", "wo", "を");
add_letter("Hiragana", "n", "ん");

// ---------------------------------------------------

add_letter("H_Diacritics", "ga", "が");
add_letter("H_Diacritics", "gi", "ぎ");
add_letter("H_Diacritics", "gu", "ぐ");
add_letter("H_Diacritics", "ge", "げ");
add_letter("H_Diacritics", "go", "ご");

add_letter("H_Diacritics", "ga", "が");
add_letter("H_Diacritics", "gi", "ぎ");
add_letter("H_Diacritics", "gu", "ぐ");
add_letter("H_Diacritics", "ge", "げ");
add_letter("H_Diacritics", "go", "ご");

add_letter("H_Diacritics", "za", "ざ");
add_letter("H_Diacritics", "ji", "じ");
add_letter("H_Diacritics", "zu", "ず");
add_letter("H_Diacritics", "ze", "ぜ");
add_letter("H_Diacritics", "zo", "ぞ");

add_letter("H_Diacritics", "da", "だ");
add_letter("H_Diacritics", "ji", "ぢ");
add_letter("H_Diacritics", "zu", "づ");
add_letter("H_Diacritics", "de", "で");
add_letter("H_Diacritics", "do", "ど");

add_letter("H_Diacritics", "ba", "ば");
add_letter("H_Diacritics", "bi", "び");
add_letter("H_Diacritics", "bu", "ぶ");
add_letter("H_Diacritics", "be", "べ");
add_letter("H_Diacritics", "bo", "ぼ");

add_letter("H_Diacritics", "pa", "ぱ");
add_letter("H_Diacritics", "pi", "ぴ");
add_letter("H_Diacritics", "pu", "ぷ");
add_letter("H_Diacritics", "pe", "ぺ");
add_letter("H_Diacritics", "po", "ぽ");

// ---------------------------------------------------

add_letter("H_Digraphs", "kya", "きゃ");
add_letter("H_Digraphs", "kyu", "きゅ");
add_letter("H_Digraphs", "kyo", "きょ");

add_letter("H_Digraphs", "sha", "しゃ");
add_letter("H_Digraphs", "shu", "しゅ");
add_letter("H_Digraphs", "sho", "しょ");

add_letter("H_Digraphs", "cha", "ちゃ");
add_letter("H_Digraphs", "chu", "ちゅ");
add_letter("H_Digraphs", "cho", "ちょ");

add_letter("H_Digraphs", "nya", "にゃ");
add_letter("H_Digraphs", "nyu", "にゅ");
add_letter("H_Digraphs", "nyo", "にょ");

add_letter("H_Digraphs", "hya", "ひゃ");
add_letter("H_Digraphs", "hyu", "ひゅ");
add_letter("H_Digraphs", "hyo", "ひょ");

add_letter("H_Digraphs", "mya", "みゃ");
add_letter("H_Digraphs", "myu", "みゅ");
add_letter("H_Digraphs", "myo", "みょ");

add_letter("H_Digraphs", "rya", "りゃ");
add_letter("H_Digraphs", "ryu", "りゅ");
add_letter("H_Digraphs", "ryo", "りょ");

// ---------------------------------------------------

add_letter("H_Digraphs_Diacritics", "gya", "ぎゃ");
add_letter("H_Digraphs_Diacritics", "gyu", "ぎゅ");
add_letter("H_Digraphs_Diacritics", "gyo", "ぎょ");

add_letter("H_Digraphs_Diacritics", "ja", "じゃ");
add_letter("H_Digraphs_Diacritics", "ju", "じゅ");
add_letter("H_Digraphs_Diacritics", "jo", "じょ");

add_letter("H_Digraphs_Diacritics", "ja (dja)", "ぢゃ");
add_letter("H_Digraphs_Diacritics", "ju (dju)", "ぢゅ");
add_letter("H_Digraphs_Diacritics", "jo (djo)", "ぢょ");

add_letter("H_Digraphs_Diacritics", "bya", "びゃ");
add_letter("H_Digraphs_Diacritics", "byu", "びゅ");
add_letter("H_Digraphs_Diacritics", "byo", "びょ");

add_letter("H_Digraphs_Diacritics", "pya", "ぴゃ");
add_letter("H_Digraphs_Diacritics", "pyu", "ぴゅ");
add_letter("H_Digraphs_Diacritics", "pyo", "ぴょ");
